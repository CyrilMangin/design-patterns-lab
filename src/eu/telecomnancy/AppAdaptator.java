package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyAdaptor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdaptor {
	
	public static void main(String[] args) {
		LegacyTemperatureSensor sensor =  new LegacyTemperatureSensor();
		ISensor sensorAdapted = new LegacyAdaptor(sensor);
		new ConsoleUI(sensorAdapted);
    }

}
