package eu.telecomnancy.sensor;

public class CapteurEtat implements ISensor{
	
	protected ISensor etat;
	protected boolean state;
	protected double value;
	
	public CapteurEtat() {
		etat = new CapteurOff();
	}

	@Override
	public void on() {
		etat.on();
		
	}

	@Override
	public void off() {
		etat.off();
		
	}

	@Override
	public boolean getStatus() {
		return etat.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		etat.update();
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return etat.getValue();
	}
	
	public void setValue(double value) throws SensorNotActivatedException {
		this.value = etat.getValue();
	}

}
