package eu.telecomnancy.sensor;

import java.util.Random;

public class CapteurOff extends CapteurEtat implements ISensor{
	
	double value;
	
	public CapteurOff() {
		state = false;
	}

	@Override
	public void on() {
		etat = new CapteurOn();		
	}

	@Override
	public void off() {
		throw new IllegalStateException("Capteur déjà non-actif");
		
	}

	@Override
	public boolean getStatus() {
		return false;
	}

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
        } else {
        	throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        }
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
	public void setValue(double value) throws SensorNotActivatedException {
		this.value = value;
	}

}
