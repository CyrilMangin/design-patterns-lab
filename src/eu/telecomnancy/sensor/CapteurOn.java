package eu.telecomnancy.sensor;

import java.util.Random;

public class CapteurOn extends CapteurEtat implements ISensor{
	
	double value;
	
	public CapteurOn() {
		state = true;
	}

	@Override
	public void on() {
		throw new IllegalStateException("Capteur déjà actif");
	}

	@Override
	public void off() {
		etat = new CapteurOff();		
	}

	@Override
	public boolean getStatus() {
		return true;
	}

    @Override
    public void update() throws SensorNotActivatedException {
        if (state) {
            value = (new Random()).nextDouble() * 100;
        } else {
        	throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        }
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
	public void setValue(double value) throws SensorNotActivatedException {
		this.value = value;
	}

}
