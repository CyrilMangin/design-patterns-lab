package eu.telecomnancy.sensor;

public abstract class ConcreteDecorateur implements ISensor{
	
	protected Decorateur tmpDeco;
	
	public ConcreteDecorateur(Decorateur newDecorateur) {
		tmpDeco = newDecorateur;
	}
	
	@Override
	public void on() {
		tmpDeco.on();		
	}

	@Override
	public void off() {
		tmpDeco.off();		
	}

	@Override
	public boolean getStatus() {
		return tmpDeco.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		tmpDeco.update();		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return tmpDeco.getValue();
	}
	

}
