package eu.telecomnancy.sensor;

public class Decorateur implements ISensor{
	
	ISensor sensor = new TemperatureSensor();
	
	public Decorateur(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public void on() {
		sensor.on();		
	}

	@Override
	public void off() {
		sensor.off();		
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

}
