package eu.telecomnancy.sensor;

public class FahrenheitDecorator extends ConcreteDecorateur{

	public FahrenheitDecorator(Decorateur newDecorateur) {
		super(newDecorateur);
		// TODO Auto-generated constructor stub
	}
	
	public double getValue() throws SensorNotActivatedException {
		return tmpDeco.getValue()*9/5+32;		
	}

 }
