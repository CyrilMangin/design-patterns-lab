package eu.telecomnancy.sensor;

public class LegacyAdaptor implements ISensor{
	
	LegacyTemperatureSensor legacyTemp;
	boolean state = false;
	
	public LegacyAdaptor(LegacyTemperatureSensor newLegacyTemp) {		
		legacyTemp = newLegacyTemp;		
	}

	@Override
	public void on() {
		if(legacyTemp.getStatus() == false) {
			state = true;
		}
		
	}

	@Override
	public void off() {
		if(legacyTemp.getStatus() == true) {
			state = false;
		}
		
	}

	@Override
	public boolean getStatus() {
		return legacyTemp.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		legacyTemp.onOff();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return legacyTemp.getTemperature();
	}

}
