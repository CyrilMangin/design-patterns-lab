package eu.telecomnancy.sensor;

public enum LogLevel {
    INFO, WARNING, ERROR, FATAL
}
