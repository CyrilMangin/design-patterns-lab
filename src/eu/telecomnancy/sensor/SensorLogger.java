package eu.telecomnancy.sensor;

public interface SensorLogger {
    public void log(LogLevel level, String message);
}
