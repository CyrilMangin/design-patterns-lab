package eu.telecomnancy.sensor;

public class SensorProxyFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
        return new SensorProxy(new TemperatureSensor(), new SimpleSensorLogger());
    }
}
