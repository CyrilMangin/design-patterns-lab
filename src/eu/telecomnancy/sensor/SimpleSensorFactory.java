package eu.telecomnancy.sensor;

public class SimpleSensorFactory extends SensorFactory {

    @Override
    public ISensor getSensor() {
        return new TemperatureSensor();
    }
}
