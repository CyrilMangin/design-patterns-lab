package eu.telecomnancy.sensor;

public class SimpleSensorLogger implements SensorLogger {
    @Override
    public void log(LogLevel level, String message) {
        System.out.println(level.name() + " " + message);
    }
}
