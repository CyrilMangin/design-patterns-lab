package eu.telecomnancy.ui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import eu.telecomnancy.sensor.CelsiusDecorator;
import eu.telecomnancy.sensor.Decorateur;
import eu.telecomnancy.sensor.FahrenheitDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.RoundDecorator;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;
    boolean state = false;
    FahrenheitDecorator Fa;
    CelsiusDecorator Ce;
    RoundDecorator Ro;
    boolean mode = true;

    private JLabel value = new JLabel("N/A Â°C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton CelsiusFahrenheit = new JButton("Convertion en Fahrenheit");
    private JButton FahrenheitCelsius = new JButton("Convertion en Celsius");
    private JButton Mode = new JButton("Mode C-F / F-C");
    private JButton Round = new JButton("Round");

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
        ((Observable)this.sensor).addObserver(this);
               
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);
        
        Fa = new FahrenheitDecorator(new Decorateur(sensor));
        Ce = new CelsiusDecorator(new Decorateur(sensor));
        Ro = new RoundDecorator(new Decorateur(sensor));

        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        CelsiusFahrenheit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                	if(mode) {
                		value.setText("value : "+Fa.getValue()+" Fahrenheit");
                		mode = false;
                	}
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        
        FahrenheitCelsius.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                	if(!mode) {
                		value.setText("value : "+Ce.getValue()+" Celsius");
                		mode = true;
                	}
				} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
        
        Mode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(mode) {
					mode = false;
				} else {
					mode = true;
				}
            }
        });
        
        Round.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	try {
	                if(mode) {	
						value.setText("value : "+Ro.getValue()+" Celsius");
					} else {
						value.setText("value : "+Ro.getValue()+" Fahrenheit");
					}
            	} catch (SensorNotActivatedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(Mode);
        buttonsPanel.add(CelsiusFahrenheit);
        buttonsPanel.add(FahrenheitCelsius);
        buttonsPanel.add(Round);
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		try {
			if(mode) {
				value.setText("value : "+Ce.getValue()+" Celsius");
			} else {
				value.setText("value : "+Fa.getValue()+" Fahrenheit");
			}
		} catch (SensorNotActivatedException e) {
			e.printStackTrace();
		}		
	}
}
