@startuml

class ConsoleUI {
 +sensor : ISensor
 +console : Scanner
__
 +ConsoleUI(Sensor)
__
 +manageCLI() : void
}

class SensorView {
 +sensor : ISensor
 +value : JLabel
 +on : JButton
 +off : JButton
 +update : JButton
__
SensorView(ISensor)
}

class MainWindow {
 +sensor : Isensor
 +sensorView : SensorView
__
 +MainWindow(ISensor)
}

interface ISensor {
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

ConsoleUI "1" *--> "1" ISensor
SensorView "1" *--> "1" ISensor 
MainWindow "1" *--> "1" ISensor
MainWindow "1" *--> "1" SensorView

class TemperatureSensor {
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

ISensor <|.. TemperatureSensor


class LegacyAdaptor {
 +legacyTemp : LegacyTemperatureSensor
__
 +LegacyAdaptor(LegacyTemperatureSensor newLegacyTemp)
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

ISensor <|.. LegacyAdaptor

class LegacyTemperatureSensor {
 -state : boolean
 -end : double
 -value : double
 -worker : AcquiringThread
 +onOff() : boolean
 +getStatus : boolean
 +getTemperature : double
}

LegacyAdaptor -> LegacyTemperatureSensor

class AcquiringThread {
 -active : boolean
 +disable : void
 +run() : void
}

LegacyTemperatureSensor <- AcquiringThread

@enduml