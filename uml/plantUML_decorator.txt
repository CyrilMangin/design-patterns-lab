@startuml

class ConsoleUI {
 +sensor : ISensor
 +console : Scanner
__
 +ConsoleUI(Sensor)
__
 +manageCLI() : void
}

class SensorView {
 +sensor : ISensor
 +value : JLabel
 +on : JButton
 +off : JButton
 +update : JButton
__
SensorView(ISensor)
__
 +update(Observable arg0, Object arg1) : void
}

class MainWindow {
 +sensor : Isensor
 +sensorView : SensorView
__
 +MainWindow(ISensor)
}

interface ISensor {
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

ConsoleUI "1" *--> "1" ISensor
SensorView "1" *--> "1" ISensor 
MainWindow "1" *--> "1" ISensor
MainWindow "1" *--> "1" SensorView

class TemperatureSensor {
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

class Observable {
 +addObserver(Observer o) : void
 -clearChanged() : void
 +countObservers() : int
 +deleteObserver(Observer o) : void
 +deleteObservers() : void
 +hasChanged() : boolean
 +notifyObservers() : void
 +notifyObservers(Object arg) : void
 -setChanged() : void
}

class Observer {
 +update(Observable o, Object arg) : void
}

SensorView ..|> Observer

class CapteurEtat {
 -etat : ISensor
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
__
 +value : double
 +state : boolean
}

ISensor <|.. CapteurEtat

class SensorProxy {
 -sensor : ISensor
 -log : SensorLogger
__
 +SensorProxy(ISensor _sensor, SensorLogger sensorLogger)
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
}

ISensor <|.. SensorProxy
CapteurEtat <- SensorProxy

enum LogLevel {
INFO
WARNING
ERROR
FATAL
}

interface SensorLogger {
 +log(LogLevel level, String message) : void
}

class SimpleSensorLogger {
 +log(LogLevel level, String message) : void
}

SimpleSensorLogger --|> SensorLogger
SimpleSensorLogger --* LogLevel
SensorLogger -left-* LogLevel
SensorProxy --* SensorLogger

class CapteurOn {
 +CapteurOn()
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
}

class CapteurOff {
 +CapteurOff()
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
}

CapteurEtat <-up- CapteurOn
CapteurEtat <-up- CapteurOff

ISensor <|.. TemperatureSensor
TemperatureSensor <-up- Observable

class Decorateur {
 +sensor : ISensor
__
 +Decorateur(ISensor sensor)
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
}

abstract class ConcreteDecorateur {
 #tmpDeco : Decorateur
__
 +ConcreteDecorateur(Decorateur newDecorateur)
__
 +on() : void
 +off() : void
 +getStatus() : boolean
 +update() : void
 +getValue() : double
}

class CelsiusDecorator {
 +CelsiusDecorator(Decorateur newDecorateur)
__
 +getValue() : double
}

class FahrenheitDecorator {
 +FahrenheitDecorator(Decorateur newDecorateur)
__
 +getValue() : double
}

class RoundDecorator {
 +RoundDecorator(Decorateur newDecorateur)
__
 +getValue() : double
}

ISensor --* ConcreteDecorateur
ISensor <|.down. ConcreteDecorateur
ConcreteDecorateur <-up- CelsiusDecorator
ConcreteDecorateur <-up- FahrenheitDecorator
ConcreteDecorateur <-up- RoundDecorator
ISensor <|.down. Decorateur

@enduml